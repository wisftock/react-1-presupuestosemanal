import React, { useEffect, useState } from 'react';
import ControlPresupuesto from './components/ControlPresupuesto';
import Formulario from './components/Formulario';
import Listado from './components/Listado';
import Pregunta from './components/Pregunta';

function App() {
  // Definir state
  const [presupuesto, setPresupuesto] = useState(0);
  const [restante, setRestante] = useState(0);
  const [mostrarPregunta, setMostrarPregunta] = useState(true);
  const [addgastos, setAddgastos] = useState([]);
  const [gasto, setGasto] = useState({});
  const [createGasto, setCreateGasto] = useState(false);
  // const addNewGasts = (gasto) => {
  //   setAddgastos([...addgastos, gasto]);
  // };
  // console.log(addgastos);
  useEffect(() => {
    if (createGasto) {
      // agrega el nuevo presupuesto
      setAddgastos([...addgastos, gasto]);
      // resta el presupuesto actual
      const presupuestoRestante = restante - gasto.cantidad;
      setRestante(presupuestoRestante);
      setCreateGasto(false);
    }
  }, [createGasto, gasto, addgastos, restante]);
  return (
    <div className='container'>
      <header>
        <h1>Presupuesto Semanal</h1>
        <div className='contenido-principal contenido'>
          {mostrarPregunta ? (
            <Pregunta
              setPresupuesto={setPresupuesto}
              setRestante={setRestante}
              setMostrarPregunta={setMostrarPregunta}
            />
          ) : (
            <div className='row'>
              <div className='one-half column'>
                <Formulario
                  setGasto={setGasto}
                  setCreateGasto={setCreateGasto}
                />
              </div>
              <div className='one-half column'>
                <Listado addgastos={addgastos} />
                <ControlPresupuesto
                  presupuesto={presupuesto}
                  restante={restante}
                />
              </div>
            </div>
          )}
        </div>
      </header>
    </div>
  );
}

export default App;
