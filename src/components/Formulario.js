import React, { useState } from 'react';
import Error from './Error';
import shortid from 'shortid';
import PropTypes from 'prop-types';

const Formulario = ({ setGasto, setCreateGasto }) => {
  const [datos, setDatos] = useState({
    nombre: '',
    cantidad: 0,
  });

  const [error, setError] = useState(false);
  const handleOnChange = (e) => {
    setDatos({ ...datos, [e.target.name]: e.target.value });
  };

  //   Agregando gastos
  const { nombre, cantidad } = datos;
  const handleSubmit = (e) => {
    e.preventDefault();
    const cantidad = parseInt(datos.cantidad);
    // validar
    if (cantidad < 1 || isNaN(cantidad) || nombre.trim() === '') {
      setError(true);
      return;
    }
    setError(false);
    // construir el gasto
    const gastos = { nombre, cantidad, id: shortid.generate() };
    // pasar el gasto al componente
    setGasto(gastos);
    setCreateGasto(true);
    // resetear el form
    setDatos({
      nombre: '',
      cantidad: 0,
    });
  };
  return (
    <form onSubmit={handleSubmit}>
      <h2>Agrega tus gastos aquí</h2>
      {error && (
        <Error mensaje='Ambos campos son obligatorios o Presupuesto incorrecto' />
      )}
      <div className='campo'>
        <label>Nombre gasto</label>
        <input
          type='text'
          className='u-full-width'
          placeholder='Ingrese nombre'
          name='nombre'
          value={nombre}
          onChange={handleOnChange}
        />
      </div>

      <div className='campo'>
        <label>Cantidad gasto</label>
        <input
          type='text'
          className='u-full-width'
          placeholder='Ingrese cantidad'
          name='cantidad'
          value={cantidad}
          onChange={handleOnChange}
        />
      </div>
      <input
        type='submit'
        className='button-primary u-full-width'
        value='Agregar Gasto'
      />
    </form>
  );
};
Formulario.propTypes = {
  setGasto: PropTypes.func.isRequired,
  setCreateGasto: PropTypes.func.isRequired,
};
export default Formulario;
