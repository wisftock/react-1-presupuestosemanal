import React from 'react';
import Gastos from './Gastos';
import PropTypes from 'prop-types';

const Listado = ({ addgastos }) => {
  return (
    <div className='gastos-realizados'>
      <h2>Listado</h2>
      {addgastos.map((gasto) => (
        <Gastos key={gasto.id} gasto={gasto} />
      ))}
    </div>
  );
};
Listado.propType = {
  addgastos: PropTypes.array.isRequired,
};
export default Listado;
