import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import Error from './Error';

const Pregunta = ({ setPresupuesto, setRestante, setMostrarPregunta }) => {
  const [cantidad, setCantidad] = useState(0);
  const [error, setError] = useState(false);
  // Leer presupuesto
  const handlePresupuesto = (e) => {
    setCantidad(parseInt(e.target.value, 10));
  };
  // Submit para definir el presupuesto
  const handleSubmit = (e) => {
    e.preventDefault();
    // validar
    if (cantidad < 1 || isNaN(cantidad)) {
      setError(true);
      return;
    }
    // si pasa la validación
    setError(false);
    setPresupuesto(cantidad);
    setRestante(cantidad);
    setMostrarPregunta(false);
  };
  return (
    <Fragment>
      <h2>Coloca tu Presupuesto</h2>
      {error ? <Error mensaje='El Presupuesto es incorrecto' /> : null}
      <form onSubmit={handleSubmit}>
        <input
          type='number'
          className='u-full-width'
          placeholder='Coloca tu presupuesto'
          onChange={handlePresupuesto}
        />
        <input
          type='submit'
          className='button-primary u-full-width'
          value='Agregar'
        />
      </form>
    </Fragment>
  );
};
Pregunta.PropType = {
  setCantidad: PropTypes.func.isRequired,
  setRestante: PropTypes.func.isRequired,
  setMostrarPregunta: PropTypes.func.isRequired,
};
export default Pregunta;
